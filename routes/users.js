var express = require('express');
var user = require('../controllers/user');
var router = express.Router();
var multer = require('multer');
var mime = require('mime');
var csrf = require('csurf');
var csrfProtection = csrf();
router.use(csrfProtection);
var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'public/uploads')
  },
  filename: function (req, file, cb) {
    cb(null, file.fieldname + '-' + Date.now() + '.' +  mime.extension(file.mimetype)) 
  }
})

var upload = multer({ storage: storage }).single('avatar');

/* GET users listing. */
router.post('/signin', user.signin);
router.post('/signup', user.signup);
router.get('/signout', isLoggedIn, user.signout);
router.get('/email-verification/:url', user.checkverify);
router.get('/userdetail/:id', isLoggedIn, user.showuserinformation);
router.get('/userdetail/:id/billinfo', isLoggedIn, user.showBillInfo);
router.post('/userdetail/:id', isLoggedIn, upload, user.editinfo);
router.get('/userdetail/:id/changepassword', isLoggedIn, user.showChangepassword);
router.post('/userdetail/:id/changepassword', isLoggedIn, user.changePassword);
router.get('/resetpassword', notLoggedIn, user.showPasswordresetrequest);
router.post('/resetpassword', notLoggedIn, user.passwordresetRequest);
router.get('/resetpassword/:id', notLoggedIn, user.showResetpassword);
router.post('/resetpassword/:id', notLoggedIn, user.resetPassword);
router.get('/404', user.errorpage);
router.post('404', user.errorpage_post);
router.get('/admin/userlist', isLoggedIn, isAdmin, user.showAdminUserList);
router.get('/admin/userlist/:id', isLoggedIn, isAdmin, user.showAdminUserDetail);
router.post('/admin/userlist/:id', isLoggedIn, isAdmin, user.AdminEditUserDetail);
module.exports = router;
function isLoggedIn(req, res, next) {
  if(req.isAuthenticated())
    return next();
res.redirect('/');
}
function isAdmin(req, res, next) {
  if(req.user.isAdmin)
    return next();
  req.flash('error', 'You do not have permission to access this page!');
  res.redirect('/404');
}

function notLoggedIn(req, res, next) {
  if(!req.isAuthenticated())
    return next();
res.redirect('/');
}