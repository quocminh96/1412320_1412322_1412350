var express = require('express');
var multer = require('multer');
var mime = require('mime');
var home = require('../controllers/home');
var products = require('../controllers/products');
var user = require('../controllers/user');
var cart = require('../controllers/cart');
var router = express.Router();
var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'public/uploads')
  },
  filename: function (req, file, cb) {
    cb(null, file.fieldname + '-' + Date.now() + '.' +  mime.extension(file.mimetype)) 
  }
})

var upload = multer({ storage: storage });
/* GET home page. */
router.get('/', home.index);
router.get('/products/:id', products.show);
router.post('/products/:id', products.loadComments);
/* GET product list page. */
router.get('/productlist', products.showProductlist);
router.post('/productlist', products.loadList);

router.get('/productlist/brand/:brand', products.showListByBrand);

router.get('/productlist/kind/:kind', products.showListByKind);

router.post('/products/comment/:id', products.addComment);
router.post('/products/addtocart/:id', cart.addToCart);

router.get('/admin/productlist', isLoggedIn, isAdmin, products.showAdminProductList);
router.post('/admin/productlist', products.loadAdminProductList);
router.get('/admin/addproduct/:id', isLoggedIn, isAdmin, products.showProduct);
router.post('/admin/addproduct/:id', isLoggedIn, isAdmin, upload.any(), products.addProduct);
router.get('/admin/editproduct/:id', isLoggedIn, isAdmin, products.showEditProduct);
router.post('/admin/editproduct/:id', isLoggedIn, isAdmin, upload.any(), products.editProduct);
router.get('/admin/removeproduct/:id', isLoggedIn, isAdmin, products.removeProduct);
router.get('/admin/addproductimage/:id', isLoggedIn, isAdmin, products.getAddprodImg);
router.post('/admin/addproductimage/:id', isLoggedIn, isAdmin, upload.single('img'), products.addprodImg);

router.post('/cart/reducebyone/:id', cart.reduceByOne);
router.post('/cart/addbyone/:id', cart.addByOne);
router.post('/cart/removeall/:id', cart.removeAll);
router.get('/cart', cart.showCart);
router.get('/cart/checkout', isLoggedIn, cart.checkout);
router.post('/cart/checkout', cart.buyNow);

module.exports = router;
function isLoggedIn(req, res, next) {
  if (req.isAuthenticated()) {
    return next();
  }
  res.redirect('/');
}

function isAdmin(req, res, next) {
  if(req.user.isAdmin)
    return next();
  req.flash('error', 'You do not have permission to access this page!');
  res.redirect('/404');
}

function notLoggedIn(req, res, next) {
  if (!req.isAuthenticated())
    return next();
  res.redirect('/');
}