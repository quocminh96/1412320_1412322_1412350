var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');
var passport = require('passport');
var flash = require('connect-flash');
var hbs = require('express-handlebars');
var mongoose = require('mongoose');
var index = require('./routes/index');
var users = require('./routes/users');
var moment = require('moment');
var passport = require('passport');
var flash = require('connect-flash');
var csrf = require('csurf');
var validator = require('express-validator');
var MongoStore = require('connect-mongo')(session);
var app = express();

mongoose.connect('mongodb://thegearsweb:ngunguoi@ds143330.mlab.com:43330/myproducts');
require('./config/passport')
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.engine( 'hbs', hbs( { 
	extname: 'hbs', 
	defaultLayout: 'layout', 
	helpers: {
		timeago: function(time){
			return moment(time).startOf('second').fromNow();
		},
    plus: function(a, b){
      return a + b;
    } 
	},
	layoutsDir: __dirname + '/views/layouts/',
	partialsDir: __dirname + '/views/partials/'
} ) );

app.set( 'view engine', 'hbs' );

//uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(validator());
app.use(cookieParser());
app.use(session(
  {
    secret: 'MySecret', 
    resave: false, 
    saveUninitialized: false,
    store: new MongoStore ({ mongooseConnection: mongoose.connection }),
    cookie: { maxAge: 60 * 60 * 1000 }
}));
app.use(flash());
app.use(passport.initialize());
app.use(passport.session());
app.use(express.static(path.join(__dirname, 'public')));
app.use(csrf());

// send crftoken to layout
app.use(function (req, res, next) {
  var token = req.csrfToken();
  res.cookie('XSRF-TOKEN', token);
  res.locals.csrfToken = token;
  next();
});

app.use(function(req, res, next) {
  res.locals.login = req.isAuthenticated();
  res.locals.user = req.user;
  res.locals.session = req.session;
  next();
});

app.use('/', index);
app.use('/', users);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
	var err = new Error('Not Found');
	err.status = 404;
	next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
