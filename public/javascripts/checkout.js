Stripe.setPublishableKey('pk_test_XfG2Zo8ttAWapfVOTaybigUs');

var $form = $('#checkout-form');

$form.submit(function (event) {
    $('#charge-error').removeClass('collapse');
    $form.find('button').prop('disabled', true);
    Stripe.card.createToken({
        number: $('#card-num').val(),
        cvc: $('#card-cvc').val(),
        exp_month: $('#ex-month').val(),
        exp_year: $('#ex-year').val(),
        name: $('#card-name').val()
    }, stripeResponseHanler);
    return false;
});

function stripeResponseHanler(status, response) {
    if (response.error) {
        $('#charge-error').text(response.error.message);
        $('#charge-error').removeClass('collapse');
        $form.find('button').prop('disabled', false);
    }
    else {
        alert("Checkout cart?");
        var token = response.id;
        $form.append($('<input type="hidden" name="stripeToken"/>').val(token));
        $form.get(0).submit();
    }
}