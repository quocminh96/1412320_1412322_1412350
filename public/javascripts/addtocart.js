function addToCart(id) {
    $.ajax({
        type: "POST",
        url: "/products/addtocart/" + id,
        dataType: 'json',
        success: function (result) {
            $('#cartQuantity').html(result.quantity);
            $('#cartQuantity').removeClass('collapse');
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr.status);
            console.log(thrownError);
        }
    });
}

function reduceByOne(id) {
    $.ajax({
        type: "POST",
        url: "/cart/reducebyone/" + id,
        dataType: "json",
        success: function (result) {
            if (result.totalQuantity <= 0) {
                $('#cartQuantity').html('0');
            }
            else {
                $('#cartQuantity').removeClass('collapse');
                $('#cartQuantity').html(result.totalQuantity);
            }

            if (result.name == null) {
                $('#list' + id).empty();
            }
            else {
                $('#quantity' + id).attr('value', result.quantity);
                $('#productPrice' + id).html('$' + result.price);
            }

            if (result.name == null) {
                $('#bill' + id).empty();
            }
            else {
                var html2 = '<span> ' + result.name + '</span> ' + result.itemPrice + ' x ' + result.quantity;
                $('#bill' + id).html(html2);
            }

            var html3 = '<h2>Total</h2>' + '<h2>$' + result.totalPrice + '</h2>';
            $('#totalPrice').html(html3);
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr.status);
            console.log(thrownError);
        }
    });
}

function removeAll(id) {
    $.ajax({
        type: "POST",
        url: "/cart/removeall/" + id,
        dataType: "json",
        success: function (result) {
            if (result.totalQuantity <= 0) {
                $('#cartQuantity').html('0');
            }
            else {
                $('#cartQuantity').html(result.totalQuantity);
                $('#cartQuantity').removeClass('collapse');
            }

            $('#list' + id).empty();
            $('#bill' + id).empty();

            var html3 = '<h2>Total</h2>' + '<h2>$' + result.totalPrice + '</h2>';
            $('#totalPrice').html(html3);
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr.status);
            console.log(thrownError);
        }
    });
}

function addByOne(id) {
    $.ajax({
        type: "POST",
        url: "/cart/addbyone/" + id,
        dataType: "json",
        success: function (result) {
            $('#cartQuantity').html(result.totalQuantity);
            $('#cartQuantity').removeClass('collapse');
            $('#quantity' + id).attr('value', result.quantity);

            $('#productPrice' + id).html('$' + result.price);

            var html2 = '<span> ' + result.name + '</span> ' + result.itemPrice + ' x ' + result.quantity;
            $('#bill' + id).html(html2);

            $('#totalPrice').empty();
            var html3 = '<h2>Total</h2>' + '<h2>$' + result.totalPrice + '</h2>';
            $('#totalPrice').html(html3);
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr.status);
            console.log(thrownError);
        }
    });
}