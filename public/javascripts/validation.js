$(document).ready(function(){

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('#signin-button').click(function(e) {
        e.preventDefault();
        //var data = $("#login-form").serialize();
        var data = {
            email    : $('#signinemail').val(),
            password    : $('#signinpassword').val(),
        };

        $.ajax({

            type: 'POST',
            data: data,
            dataType: 'json',
            url: '/signin',

            success: function(result){
                if (result.hasOwnProperty('error')){
                    $('#signin-tab').find('.alert-danger').html('<i class="fa-fw fa fa-warning"></i> ' + result.error).removeClass('collapse');
                    $('#signin-tab').find('.alert-success').addClass('collapse');
                } else {
                    $('#signin-tab').find('.alert-success').html('<i class="fa-fw fa fa-check-circle"></i> Successfully Sign-in').removeClass('collapse');
                    $('#signin-tab').find('.alert-danger').addClass('collapse');
                    // 4 giay sau sẽ tắt popup
                    setTimeout(function(){
                        $('#flipFlop').modal('hide');
                        // Ẩn thông báo lỗi
                        $('#signin-tab').find('.alert-danger').addClass('collapse');
                        $('#signin-tab').find('.alert-success').addClass('collapse');
                    }, 1000);
                    window.location= result.redirect;
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(thrownError);
            }
        });
    });

    $('#signup-button').click(function(e) {
        e.preventDefault();
        //var data = $("#login-form").serialize();
        var data = {
            email    : $('#signupemail').val(),
            password    : $('#signuppassword').val(),
            confirmpassword    : $('#confirmpassword').val(),
            name    : $('#name').val(),
            address    : $('#address').val(),
            phone    : $('#phone').val(),
            avatar    : $('#avatar').val(),
        };

        $.ajax({

            type: 'POST',
            data: data,
            dataType: 'json',
            url: '/signup',

            success: function(result){
                if (result.hasOwnProperty('error')){
                    if (result.step == '1') {
                        $("#previous-button").click();
                        $('#signup-tab').find('.alert-danger').html('<i class="fa-fw fa fa-warning"></i> ' + result.error).removeClass('collapse');
                        $('#signup-tab').find('.alert-success').addClass('collapse');
                    } else {
                        $('#signup-tab').find('.alert-danger').html('<i class="fa-fw fa fa-warning"></i> ' + result.error).removeClass('collapse');
                        $('#signup-tab').find('.alert-success').addClass('collapse');
                    }
                } else {
                    $('#signup-tab').find('.alert-success').html('<i class="fa-fw fa fa-check-circle"></i> ' + result.success).removeClass('collapse');
                    $('#signup-tab').find('.alert-danger').addClass('collapse');
                    // 4 giay sau sẽ tắt popup
                    setTimeout(function(){
                        $('#flipFlop').modal('hide');
                        // Ẩn thông báo lỗi
                        $('#signup-tab').find('.alert-danger').addClass('collapse');
                        $('#signup-tab').find('.alert-success').addClass('collapse');
                    }, 2000);
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(thrownError);
            }
        });
    });
});
