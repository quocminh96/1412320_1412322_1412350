function addComment(id) {
    $.ajax({
        type: "POST",
        url: "/products/comment/" + id,
        data: {
            username: $('#username').val(),
            content: $('#content').val()
        },
        dataType: 'json'
    }).done(function(commentResult) {
        if (commentResult.hasOwnProperty('error')) {
            $('.form-comment').find('.alert-danger').html('<i class="fa-fw fa fa-warning"></i>' + commentResult.error).removeClass('collapse');
            $('.form-comment').find('.alert-success').addClass('collapse');
        }
        else
        {
            $('.form-comment').find('.alert-success').html('<i class="fa-fw fa fa-check-circle"></i> Successfully commenting').removeClass('collapse');
            $('.form-comment').find('.alert-danger').addClass('collapse');
            var html ='';
            html +='<div class="row new-comment-content">'
            + ' <div class="col-md-1 avatar">'
            + '<img src="/images/defaultavatar.png" alt="Avatar">'
            + '</div>' 
            + '<div class="col-md-10">'
            + '<div class="review">'
            + '<div class="username">'
            + commentResult.username
            + '</div>'
            + '<i class="fa-fw fa fa-star yellow-star"></i>'
            + '<i class="fa-fw fa fa-star yellow-star"></i>'
            + '<i class="fa-fw fa fa-star yellow-star"></i>'
            + '<i class="fa-fw fa fa-star-o"></i>'
            + '<i class="fa-fw fa fa-star-o"></i>'
            + '<div class="comment">'
            + commentResult.content
            + '</div>'
            + '<div class="post-time">'
            + '<i class="fa-fw fa fa-clock-o"></i>'
            + commentResult.time
            + '</div>'
            + '</div>'
            + '</div>'
            + '<div class="col-md-1">'
            + '<span>New</span>'
            + '</div>'
            + '</div>';
            $('#new-comment-block').append(html);
        }
    });
}