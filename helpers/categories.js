var Product = require('../models/products');

module.exports = function (object, callback) {
    Product.findAllBrand().then(function (docs) {
        object['brands'] = docs;
        Product.findAllKind().then(function (docs) {
            object['kinds'] = docs;
            callback();
        })
    })
}