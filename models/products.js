var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var schema = new Schema(
{
	name: {type: String},
	brand: {type: String},
	subcategories: {type: [String]},
	cpu: {type: String},
	ram: {type: String},
	weight: {type: String},
	resolution: {type: String},
	description: {type: [String]},
	rom: {type: String},
	price: {type: String},
	rating: {type: Number},
	gpu: {type: String},
	image: {type: [String]},
	quantity: {type: Number},
	kind: {type: String}
});

schema.statics.findAll = function() {
	return this.find({});
}

schema.statics.findPerPage = function(filter, limit, start) {
	return this.find(filter).skip(start).limit(limit);
};

schema.statics.findAllBrand = function() {
	return this.find().distinct('brand');
}

schema.statics.findAllKind = function() {
	return this.find().distinct('kind');
}

schema.statics.findAllSubcategories = function() {
	return this.find().distinct('subcategories');
}

schema.statics.findByBrand = function(brand) {
	return this.find({brand:brand});
}

schema.statics.findByKind = function(kind) {
	return this.find({kind:kind});
}

schema.statics.findBySubcategories = function(subcategories) {
	return this.find({subcategories:subcategories})
}
schema.statics.findALlSubcategoriesByKind = function(kind) {
	return this.find({kind: kind}).distinct('subcategories');
}

module.exports = mongoose.model('Product', schema);