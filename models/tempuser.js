var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var bcrypt = require('bcrypt-nodejs');
var schema = new Schema(
{
	name: {type: String, required: true},
	email: {type: String, required: true},
	address: {type: String, required: true},
	phone: {type: String, required: true},
	password: {type: String, required: true},
	avatar: {type: String, required: true},
	address: {type: String, required: true},
	isAdmin: {type: Boolean, requied: true},
	GENERATED_VERIFYING_URL: {type: String}
});

schema.methods.encryptPassword = function(password) {
	return bcrypt.hashSync(password, bcrypt.genSaltSync(5), null);
};

schema.methods.validPassword = function(password) {
	return bcrypt.compareSync(password, this.password);
};
module.exports = mongoose.model('TempUser', schema);