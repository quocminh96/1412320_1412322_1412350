var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var schema = new Schema(
{
	username: {type: String, required: true},
	content: {type: String, required: true},
	time: {type: Date, required: true},
	productId: {type: String, required: true}
});

schema.statics.findAll = function() {
	return this.find({});
};

schema.statics.findPerPage = function(limit, start, productId) {
	return this.find({productId: productId}).sort({time: -1}).skip(start).limit(limit);
};

schema.statics.findByProductId = function(productId) {
	return this.find({productId: productId});
}

module.exports = mongoose.model('Comment', schema);