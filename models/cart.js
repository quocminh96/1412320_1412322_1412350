var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var schema = new Schema(
    {
        items: { type: Schema.Types.Mixed, required: true },
        totalQuantity: { type: Number, required: true },
        totalPrice: { type: Number, required: true },
    });

schema.methods.add = function (item, id) {
    var storedItem = this.items[id];
    if (!this.items[id]) {
        this.items[id] = {
            item: item,
            quantity: 1,
            price: parseInt(item.price ,10)
        };
        storedItem = this.items[id];
    } else {
        storedItem.quantity++;
        storedItem.price = parseInt(storedItem.item.price * storedItem.quantity, 10);
    }
    this.totalQuantity++;
    this.totalPrice += parseInt(storedItem.item.price, 10);
}

schema.methods.addByOne = function (id) {
    this.items[id].quantity++;
    this.items[id].price += parseInt(this.items[id].item.price, 10);
    this.totalQuantity++;
    this.totalPrice += parseInt(this.items[id].item.price, 10);
}

schema.methods.generateArray = function () {
    var array = [];
    for (var id in this.items) {
        array.push(this.items[id]);
    }
    return array;
}

schema.methods.reduceByOne = function (id) {
    this.items[id].quantity--;
    this.items[id].price -= this.items[id].item.price;
    this.totalQuantity--;
    this.totalPrice -= this.items[id].item.price;

    if (this.items[id].quantity <= 0) {
        delete this.items[id];
    }
}

schema.methods.removeAll = function (id) {
    this.totalQuantity -= this.items[id].quantity;
    this.totalPrice -= this.items[id].price;
    delete this.items[id];
}

schema.methods.removeAllCart = function () {
    this.totalPrice = 0;
    this.totalQuantity = 0;
    this.items = {};
}

module.exports = mongoose.model('Cart', schema);