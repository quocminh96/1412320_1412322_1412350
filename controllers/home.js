module.exports = {
	index: function (req, res) {
		var successMsg = req.flash('success')[0];
		res.render('index', { title: 'The Gears Shop'});
	}
}