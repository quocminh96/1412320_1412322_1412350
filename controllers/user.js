var express = require('express');
var router = express.Router();
var User = require('../models/user');
var Cart = require('../models/cart');
var Order = require('../models/orders');
var multer = require('multer');
var csrf = require('csurf');
var passport = require('passport');
var verification = require('../config/email-verification-config');
var resetpass = require('../config/Reset-password-config');
var csrfProtection = csrf();
var kindHelper = require('../helpers/kind');
router.use(csrfProtection);

module.exports = {
    signin: function (req, res, next) {
        if(req.body.email.length == 0) {
            return res.json({error: 'Email must not be empty'});
        }
        if(req.body.password.length == 0) {
            return res.json({error: 'Password must not be empty'});
        }
        passport.authenticate('local.signin', function(err, user, info) {
            if (err) return next(err);
            if (!user) {
                return res.json({error: 'Invalid account'});
            }
            if (user) {
                req.logIn(user, function(err) {
                    if (err) {
                        return res.json({error: err}); 
                    }
                    if (user.isAdmin)
                    {

                       return res.json({redirect: '/admin/productlist'});
                   }
                   else
                   {
                    return res.json({redirect: '/'});
                }
            });
            }
        })(req, res, next);
    },

    signup: function (req, res, next) {
        if (req.body.email.length == 0) {
            return res.json({error: 'Email must not be empty', step: '1'});
        }
        if (req.body.password.length == 0) {
            return res.json({error: 'Password must not be empty', step: '1'});
        }
        if (req.body.password.length < 6) {
            return res.json({error: 'Password must have at least 6 characters', step: '1'});
        }
        if (req.body.confirmpassword.length == 0) {
            return res.json({error: 'Confirm password must not be empty', step: '1'});
        }
        if (req.body.password != req.body.confirmpassword) {
            return res.json({error: 'Confirm password does not match', step: '1'});
        }
        if (req.body.name.length == 0) {
            return res.json({error: 'Full name must not be empty'});
        }
        if (req.body.address.length == 0) {
            return res.json({error: 'Address must not be empty'});
        }
        if (req.body.phone.length == 0) {
            return res.json({error: 'Phone must not be empty'});
        }
        if (req.body.avatar.length == 0) {
            return res.json({error: 'Please choose your Avatar'});
        }
        passport.authenticate('local.signup', function(err, user, info) {
            if (err) return next(err);
            if (!user) {
                return res.json({error: 'This email address is already in use', step: '1'});
            }
            if (user) {
                return res.json({success: user});
            }
        })(req, res, next);
    },

    signout: function (req, res) {
        var cart = Cart();
        cart.removeAllCart();
        req.session.cart = cart;
        req.logout();
        res.redirect('/');
    },

    checkverify: function(req, res) {
        verification.checkverify(req).then(function(result){
            req.logIn(result, function(err) {
                if (err) {
                    return res.json({error: err}); 
                }
                res.redirect('/');
            });
        })
    },

    showuserinformation: function(req, res) { 
        if ((req.params.id != req.user.id) && (!req.user.isAdmin))
        {
            req.flash('error', 'You do not have permission to see other user profile');
            res.redirect('/404');
        }
        else   
            res.render('userdetail');
    },
    
    editinfo: function(req, res){
        if ((req.params.id != req.user.id) && (!req.user.isAdmin))
        {
            req.flash('error', 'You do not have permission to edit other user profile');
            res.redirect('/404');        
        }
        else   
        {
            var user;
            user = new User(
            {
                _id : req.params.id,
                name : req.body.name,
                email: req.body.email,
                address: req.body.address,
                phone: req.body.phone,
                password: req.user.password,
                isAdmin: req.user.isAdmin,
                avatar: '/uploads/' + req.file.filename 
            });
            console.log(req.file.path);
            User.findByIdAndUpdate(req.params.id, user, {}, function (err) {
                if (err) { return next(err); }
                res.redirect('/userdetail/' + req.params.id);
            });
        }
    },
    showChangepassword: function(req,res){
        if ((req.params.id != req.user.id) && (!req.user.isAdmin))
        {
            req.flash('error', 'You do not have permission to change other users password!');
            res.redirect('/404');
        }
        else 
        {
            var messages = req.flash('error');
            res.render('changepassword', {messages: messages, hasErrors: messages.length > 0});
        }
    },
    changePassword: function(req, res){
        if ((req.params.id != req.user.id) && (!req.user.isAdmin))
        {
            req.flash('error', 'You do not have permission to change other users password!');
            res.redirect('/404');
        }
        else 
        {
            var user = req.user;
            if (!user.validPassword(req.body.oldpass))
            {
                req.flash('error', 'Invalid old password!');
                res.redirect('/userdetail/' + req.params.id + '/changepassword');
            }
            else
            {
                if (req.body.newpass != req.body.cnewpass)
                {
                    req.flash('error', 'Confirm password does not match!');
                    res.redirect('/userdetail/' + req.params.id + '/changepassword');
                    return;
                }
                else
                {
                    if (req.body.newpass.length < 6) {
                        req.flash('error', 'Password must have at least 6 characters');
                        res.redirect('/userdetail/' + req.params.id +'/changepassword');
                        return;
                    }
                    var user;
                    user = new User(
                    {
                        _id : req.params.id,
                        password: req.body.newpass
                    });
                    console.log(req.body.newpass);
                    user.password = user.encryptPassword(req.body.newpass);
                    User.findByIdAndUpdate(req.params.id, user, {}, function (err) {
                        if (err) { return next(err); }
                        res.redirect('/userdetail/' + req.params.id);
                    });
                }
            }
        }
    },
    showPasswordresetrequest: function(req, res){
        var errors = req.flash('error');
        var infos = req.flash('info');   
        res.render('passwordresetrequest', {errors: errors, infos: infos, hasErrors: errors.length > 0, hasInfos: infos.length > 0});
    },
    passwordresetRequest: function(req, res){
        resetpass.sendResetPassEmail(req.body.email).then(function(result){
            req.flash('info', result);
            res.redirect('/resetpassword');
        }).catch(function(err){
            req.flash('error', err);
            res.redirect('/resetpassword');
        });
    },
    showResetpassword: function(req, res){
        var errors = req.flash('error');   
        res.render('resetpassword', {errors: errors, hasErrors: errors.length > 0, id : req.params.id});
    },
    resetPassword: function(req, res){
        if (req.body.newpass != req.body.cnewpass)
        {
            req.flash('error', 'Confirm password does not match!');
            res.redirect('/resetpassword/' + req.params.id);
        }
        else
        {
            if (req.body.newpass.length < 6) {
                req.flash('error', 'Password must have at least 6 characters');
                res.redirect('/resetpassword/' + req.params.id);
            }
            else
            {
                User.findOne({resetPasswordToken: req.params.id}, function(err, docs){
                    if (err)
                    {
                        req.flash('error', err);
                        res.redirect('/resetpassword/' + req.params.id);
                    }
                    else
                    {
                        if (!docs)
                        {
                            req.flash('error', 'Invalid reset password link!');
                            res.redirect('/resetpassword/' + req.params.id);
                        }
                        else
                        {
                            docs.password = docs.encryptPassword(req.body.newpass);
                            docs.resetPasswordToken = null; 
                            docs.save();
                            res.redirect('/');
                        }
                    }
                });
            }
        }
    },
    errorpage: function(req, res){
        var messages = req.flash('error');
        res.render('404', {messages: messages});
    },
    errorpage_post: function(req, res){
        res.redirect('/');
    },
    showBillInfo: function(req, res) {
        var messages = req.flash('success');
        Order.find({user: req.user}, function(err, orders) {
            if (err) {
                return res.write('Error!');
            }
            orders.forEach(function(order) {
                var cart = new Cart();
                cart.items = order.cart.items;
                cart.totalQuantity = order.cart.totalQuantity;
                cart.totalPrice = order.cart.totalPrice;
                order.items = cart.generateArray();
            });
            res.render('billinfo', {successMsg: messages, noMessage: messages.length <= 0, orders: orders});
        });       
    },
    showAdminUserList: function(req, res) {
        User.find({}, function (err, users) {
            if (err){
                console.log(err);
                return;
            }
            var object = {users: users, layout: 'admin-layout.hbs'};
            kindHelper(object, function(){
                res.render('admin/admin-userlist', object);
            })
        });
    },
    showAdminUserDetail: function(req, res) {
        User.findById(req.params.id).then(function (docs) {
            var object ={layout: 'admin-layout.hbs', user: docs};
            kindHelper(object, function(){
                res.render('admin/admin-userdetail', object);
            })
        })
    },
    AdminEditUserDetail: function(req, res) {
        var user;
        user = new User(
        {
            _id : req.params.id,
            name : req.body.name,
            email: req.body.email,
            address: req.body.address,
            phone: req.body.phone,
            password: req.user.password,
            isAdmin: req.body.isAdmin,
            avatar: req.body.avatar
        });
        User.findByIdAndUpdate(req.params.id, user, {}, function (err) {
            if (err) 
            {
             return next(err); 
         }
         res.redirect('/admin/userlist/' + req.params.id);
     });
    },
};
