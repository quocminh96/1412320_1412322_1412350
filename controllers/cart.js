var Cart = require('../models/cart');
var Product = require('../models/products');
var Order = require('../models/orders');
module.exports = {
    addToCart: function (req, res) {
        var productId = req.params.id;
        if (req.session.cart != null && req.session.cart.totalQuantity > 0) {
            var cart = new Cart();
            cart.items = req.session.cart.items;
            cart.totalQuantity = req.session.cart.totalQuantity;
            cart.totalPrice = req.session.cart.totalPrice;
            console.log(req.session.cart.items);
        }
        else {
            var cart = new Cart({
                totalPrice: 0,
                items: {},
                totalQuantity: 0
            });
        }
        Product.findById(productId, function (err, product) {
            if (err) {
            }
            cart.add(product, productId);
            req.session.cart = cart;
            return res.json({ quantity: cart.totalQuantity });
        })
    },

    showCart: function (req, res) {
        var object = {};
        if (!req.session.cart) {
            return res.render('cart');
        }
        var cart = new Cart({
            totalPrice: req.session.cart.totalPrice,
            items: req.session.cart.items,
            totalQuantity: req.session.cart.totalQuantity
        });
        object['products'] = cart.generateArray();
        object['totalPrice'] = cart.totalPrice;
        object['totalQuantity'] = cart.totalQuantity;
        return res.render('cart', object);
    },

    reduceByOne: function (req, res) {
        var productId = req.params.id;
        if (req.session.cart) {
            var cart = new Cart();
            cart.items = req.session.cart.items;
            cart.totalQuantity = req.session.cart.totalQuantity;
            cart.totalPrice = req.session.cart.totalPrice;
        }
        else {
            var cart = new Cart({
                totalPrice: 0,
                items: {
                    item: null,
                    quantity: 0,
                    price: 0
                },
                totalQuantity: 0
            });
        }
        Product.findById(productId, function () {
            cart.reduceByOne(productId);
            req.session.cart = cart;
            if (cart.items[productId] == null) {
                return res.json({
                    quantity: 0, price: 0,
                    totalPrice: cart.totalPrice, totalQuantity: cart.totalQuantity, name: null
                });
            }
            else {
                return res.json({
                    itemPrice: cart.items[productId].item.price, quantity: cart.items[productId].quantity, price: cart.items[productId].price, productPrice: cart.items[productId].item.price,
                    totalPrice: cart.totalPrice, totalQuantity: cart.totalQuantity, name: cart.items[productId].item.name
                });
            }
        })
    },

    removeAll: function (req, res) {
        var productId = req.params.id;
        if (req.session.cart) {
            var cart = new Cart();
            cart.items = req.session.cart.items;
            cart.totalQuantity = req.session.cart.totalQuantity;
            cart.totalPrice = req.session.cart.totalPrice;
        }
        else {
            var cart = new Cart({
                totalPrice: 0,
                items: {
                    item: null,
                    quantity: 0,
                    price: 0
                },
                totalQuantity: 0
            });
        }
        Product.findById(productId, function () {
            cart.removeAll(productId);
            req.session.cart = cart;
            return res.json({
                quantity: 0, price: 0,
                totalPrice: cart.totalPrice, totalQuantity: cart.totalQuantity, name: null
            });
        })
    },

    addByOne: function (req, res) {
        var productId = req.params.id;
        var cart = new Cart();
        cart.items = req.session.cart.items;
        cart.totalQuantity = req.session.cart.totalQuantity;
        cart.totalPrice = req.session.cart.totalPrice;
        Product.findById(productId, function (err, product) {
            cart.addByOne(productId);
            req.session.cart = cart;
            return res.json({
                itemPrice: cart.items[productId].item.price, quantity: cart.items[productId].quantity, price: cart.items[productId].price, productPrice: cart.items[productId].item.price,
                totalPrice: cart.totalPrice, totalQuantity: cart.totalQuantity, name: cart.items[productId].item.name
            });
        });
    },

    checkout: function (req, res) {
        if (req.session.cart.totalQuantity <= 0) {
            return res.redirect('/cart');
        }
        var cart = new Cart();
        cart.items = req.session.cart.items;
        cart.totalQuantity = req.session.cart.totalQuantity;
        cart.totalPrice = req.session.cart.totalPrice;
        var errMsg = req.flash('error')[0];
        res.render('checkout', { total: cart.totalPrice, errMsg: errMsg, noErrors: !errMsg });
    },

    buyNow: function (req, res) {
        if (req.session.cart == null || req.session.cart.totalQuantity <= 0) {
            return res.redirect('/cart');
        }
        var cart = new Cart();
        cart.items = req.session.cart.items;
        cart.totalQuantity = req.session.cart.totalQuantity;
        cart.totalPrice = req.session.cart.totalPrice;
        var stripe = require("stripe")(
            "sk_test_gA4Dxikn6fIxLKdvpI0oK8if"
        );
        var token = req.body.stripeToken;

        var charge = stripe.charges.create({
            amount: cart.totalPrice * 100,
            currency: "usd",
            description: "Example charge",
            source: token,
        }, function (err, charge) {
            if (err) {
                req.flash('error', err.message);
                return res.redirect('/cart/checkout');
            }
            var order = new Order({
                user: req.user,
                cart: cart,
                address: req.body.address,
                name: req.body.name,
                paymentId: charge.id
            });
            order.save(function (err, result) {
                cart.removeAllCart();
                req.session.cart = cart;
                req.flash('success', 'Your order has been placed');
                req.cart = null;
                res.redirect('/userdetail/' + req.user.id + '/billinfo');
            });
        });
    }
};