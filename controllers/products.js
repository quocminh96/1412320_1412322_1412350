var Product = require('../models/products');
var Comment = require('../models/comments');
var Helper = require('../helpers/categories');
var kindHelper = require('../helpers/kind');
var moment = require('moment');
var mongoose = require('mongodb').MongoClient;
var objectId = require('mongodb').ObjectId;
var assert = require('assert');

module.exports = {
    show: function (req, res) {
        Product.findById(req.params.id).then(function (docs) {
            var object = {product: docs, isLaptop: docs.kind == "Laptop"};
            res.render('productdetail', object);           
        })
    },

    loadComments: function (req, res) {

        var pag_content = '';
        var pag_navigation = '';
        var page = parseInt(req.body.page);
        var cur_page = page;
        page -= 1;
        var per_page = 5; 
        var previous_btn = true;
        var next_btn = true;
        var first_btn = true;
        var last_btn = true;
        var start = page * per_page;

        var count;
        Comment.findPerPage(per_page, start, req.body.id).then(function (docs) {

            Comment.count({productId: req.body.id}, function(err, total){
                count = total;
                docs.forEach(function(element, key, array) {
                    pag_content += '<div class="row comment-content">' +
                    '<div class="col-md-1 avatar">' +
                    '<img src="/images/defaultavatar.png" alt="Avatar">' +
                    '</div>' +
                    '<div class="col-md-11">' +
                    '<div class="review">' +
                    '<div class="username">' +
                    docs[key].username +
                    '</div>' +
                    '<i class="fa-fw fa fa-star yellow-star"></i>' +
                    '<i class="fa-fw fa fa-star yellow-star"></i>' +
                    '<i class="fa-fw fa fa-star yellow-star"></i>' +
                    '<i class="fa-fw fa fa-star-o"></i>' +
                    '<i class="fa-fw fa fa-star-o"></i>' +
                    '<div class="comment">' +
                    docs[key].content +
                    '</div>' +
                    '<div class="post-time">' +
                    '<i class="fa-fw fa fa-clock-o"></i>' + 
                    timeago(docs[key].time) +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '</div>';
                });

                var no_of_paginations = Math.ceil(count / per_page);

                var start_loop, end_loop;

                if (cur_page >= 7) {
                    start_loop = cur_page - 3;
                    if (no_of_paginations > cur_page + 3)
                        end_loop = cur_page + 3;
                    else if (cur_page <= no_of_paginations && cur_page > no_of_paginations - 6) {
                        start_loop = no_of_paginations - 6;
                        end_loop = no_of_paginations;
                    } else {
                        end_loop = no_of_paginations;
                    }
                } else {
                    start_loop = 1;
                    if (no_of_paginations > 7)
                        end_loop = 7;
                    else
                        end_loop = no_of_paginations;
                }

                pag_navigation += "<ul>";

                if (first_btn && cur_page > 1) {
                    pag_navigation += "<li p='1' class='active'>First</li>";
                } else if (first_btn) {
                    pag_navigation += "<li p='1' class='inactive'>First</li>";
                } 

                if (previous_btn && cur_page > 1) {
                    pre = cur_page - 1;
                    pag_navigation += "<li p='" + pre + "' class='active'>Previous</li>";
                } else if (previous_btn) {
                    pag_navigation += "<li class='inactive'>Previous</li>";
                }
                for (i = start_loop; i <= end_loop; i++) {

                    if (cur_page == i)
                        pag_navigation += "<li p='" + i + "' class = 'selected' >" + i + "</li>";
                    else
                        pag_navigation += "<li p='" + i + "' class='active'>" + i + "</li>";
                }

                if (next_btn && cur_page < no_of_paginations) {
                    nex = cur_page + 1;
                    pag_navigation += "<li p='" + nex + "' class='active'>Next</li>";
                } else if (next_btn) {
                    pag_navigation += "<li class='inactive'>Next</li>";
                }

                if (last_btn && cur_page < no_of_paginations) {
                    pag_navigation += "<li p='" + no_of_paginations + "' class='active'>Last</li>";
                } else if (last_btn) {
                    pag_navigation += "<li p='" + no_of_paginations + "' class='inactive'>Last</li>";
                }

                pag_navigation = pag_navigation + "</ul>";

                var response = {
                    'content': pag_content,
                    'navigation' : pag_navigation
                };

                res.json(response);
            })
})
},
showProductlist: function (req, res) {
    var object = {};
    Product.findAll().then(function (docs) {
            //object['products'] = docs;
            Helper(object, function () {
                res.render('productlist', object);
            })
        })
},

loadList: function (req, res) {

    var pag_content = '';
    var pag_navigation = '';

    var page = parseInt(req.body.page);
    var cur_page = page;
    page -= 1;
    var per_page = 9; 
    var previous_btn = true;
    var next_btn = true;
    var first_btn = true;
    var last_btn = true;
    var start = page * per_page;

    var where_search = {};
        //Find by kind
        if (req.body.id != 'All') {
            //console.log(req.body.id);
            kind = req.body.id;
            var filter = new RegExp(kind, 'i');
            where_search = {
                '$or' : [
                {'kind' : filter},
                {'brand' : filter},
                ]
            }
            console.log(where_search);
        } else {
            //console.log('ngon');
            
            //search = req.body.search;
            namesearch = req.body.namesearch;
            brandsearch = req.body.brandsearch;
            pricesearch = req.body.pricesearch;
            cpusearch = req.body.cpusearch;
            ramsearch = req.body.ramsearch;
            romsearch = req.body.romsearch;
            resolutionsearch = req.body.resolutionsearch;

            /* Check if there is a string inputted on the search box */
            if( namesearch != '' || brandsearch != '' || pricesearch != '' || cpusearch != '' || ramsearch != '' || romsearch != ''  ||  resolutionsearch != ''){
                /* If a string is inputted, include an additional query logic to our main query to filter the results */
                //var filter = new RegExp(search, 'i'); //So khớp không phân biệt hoa thường
                var namefilter = new RegExp(namesearch, 'i');
                var brandfilter = new RegExp(brandsearch, 'i');
                var pricefilter = new RegExp(pricesearch, 'i');
                var cpufilter = new RegExp(cpusearch, 'i');
                var ramfilter = new RegExp(ramsearch, 'i');
                var romfilter = new RegExp(romsearch, 'i');
                var resolutionfilter = new RegExp(resolutionsearch, 'i');
                where_search = {
                    '$and' : [
                    {'name' : namefilter},
                    {'brand' : brandfilter},
                    {'price' : pricefilter},
                    {'cpu' : cpufilter},
                    {'ram' : ramfilter},
                    {'rom' : romfilter},
                    {'resolution' : resolutionfilter},
                    ]
                }
                console.log(where_search);

            }
        }
        var count;

        Product.findPerPage(where_search, per_page, start).then(function (docs) {
            Product.count(where_search, function(err, total){
                count = total;
                docs.forEach(function(element, key, array) {
                    pag_content += '<div class="col-md-4 text-center">' +
                    '<div class="product">' +
                    '<div class="product-image">' +
                    '<img src="' + docs[key].image[0] + '" alt="Product Image" class="mx-auto d-block">' +
                    '</div>' +               
                    '<div class="middle">' +
                    '<button class="button"><a href="/products/' + docs[key]._id + '">Detail </a></button>' +
                    '</div>' +
                    '</div>' +
                    '<p class="product-name">' + docs[key].name + '</p>' +
                    '<p class="product-brand">' + docs[key].brand + '</p>' +
                    '<p class="product-price">$' + docs[key].price + '</p>' +
                    '<div class="form-inline add-to-cart d-flex justify-content-center">' +
                    '<button onclick="addToCart(\'' + docs[key]._id + '\')" class="btn btn-primary btn-round"><i class="fa-fw fa fa-shopping-cart"></i> Add to Cart</button>' +
                    '</div>' +
                    '</div>';
                });

                var no_of_paginations = Math.ceil(count / per_page);

                var start_loop, end_loop;

                if (cur_page >= 7) {
                    start_loop = cur_page - 3;
                    if (no_of_paginations > cur_page + 3)
                        end_loop = cur_page + 3;
                    else if (cur_page <= no_of_paginations && cur_page > no_of_paginations - 6) {
                        start_loop = no_of_paginations - 6;
                        end_loop = no_of_paginations;
                    } else {
                        end_loop = no_of_paginations;
                    }
                } else {
                    start_loop = 1;
                    if (no_of_paginations > 7)
                        end_loop = 7;
                    else
                        end_loop = no_of_paginations;
                }

                pag_navigation += "<ul>";

                if (first_btn && cur_page > 1) {
                    pag_navigation += "<li p='1' class='active'>First</li>";
                } else if (first_btn) {
                    pag_navigation += "<li p='1' class='inactive'>First</li>";
                } 

                if (previous_btn && cur_page > 1) {
                    pre = cur_page - 1;
                    pag_navigation += "<li p='" + pre + "' class='active'>Previous</li>";
                } else if (previous_btn) {
                    pag_navigation += "<li class='inactive'>Previous</li>";
                }
                for (i = start_loop; i <= end_loop; i++) {

                    if (cur_page == i)
                        pag_navigation += "<li p='" + i + "' class = 'selected' >" + i + "</li>";
                    else
                        pag_navigation += "<li p='" + i + "' class='active'>" + i + "</li>";
                }

                if (next_btn && cur_page < no_of_paginations) {
                    nex = cur_page + 1;
                    pag_navigation += "<li p='" + nex + "' class='active'>Next</li>";
                } else if (next_btn) {
                    pag_navigation += "<li class='inactive'>Next</li>";
                }

                if (last_btn && cur_page < no_of_paginations) {
                    pag_navigation += "<li p='" + no_of_paginations + "' class='active'>Last</li>";
                } else if (last_btn) {
                    pag_navigation += "<li p='" + no_of_paginations + "' class='inactive'>Last</li>";
                }

                pag_navigation = pag_navigation + "</ul>";

                var response = {
                    'content': pag_content,
                    'navigation' : pag_navigation
                };

                res.json(response);
            })
        })
},

showListByBrand: function (req, res) {
    var tobject = {};
    Product.findByBrand(req.params.brand).then(function (docs) {
        object['products'] = docs;
        Helper(object, function () {
            res.render('productlist', object);
        })
    })
},

showListBySubcategories: function (req, res) {
    var object = {};
    Product.findBySubcategories(req.params.subcategories).then(function (docs) {
        object['products'] = docs;
        Helper(object, function () {
            res.render('productlist', object);
        })
    })
},

showListByKind: function (req, res) {
    var object = {};
    Product.findByKind(req.params.kind).then(function (docs) {
        object['products'] = docs;
        Helper(object, function () {
            res.render('productlist', object);
        })
    })
},

addComment: function (req, res) {
    if (req.body.username.length == '0') {
        return res.json({ error: 'Name must not be empty' });
    }
    if (req.body.content.length == '0') {
        return res.json({ error: 'Comment must not be empty' });
    }
    var comment = new Comment({
        username: req.body.username,
        content: req.body.content,
        time: new Date().getTime(),
        productId: req.params.id
    });

    comment.save().then(function (docs) {
        console.log('done');
        res.json({ username: docs.username, content: docs.content, time: timeago(docs.time) });
    });
},

showAdminProductList: function(req, res) {
    if (req.user.isAdmin)
    {
        Product.findAll().then(function(docs) {
            var object = {products: docs, layout: 'admin-layout.hbs'};
            kindHelper(object, function(){
                res.render('admin/admin-productlist', object);    
            })
        });
    }
    else
    {
     req.flash('error', 'You do not have permission to access this page!');
     res.redirect('/404');
 }
},

loadAdminProductList: function(req, res) {

    var pag_content = '';
    var pag_navigation = '';
    var page = parseInt(req.body.page);
    var cur_page = page;
    page -= 1;
    var per_page = 10; 
    var previous_btn = true;
    var next_btn = true;
    var first_btn = true;
    var last_btn = true;
    var start = page * per_page;

    var count;
    Product.findPerPage({}, per_page, start).then(function (docs) {

        Product.count({}, function(err, total){
            count = total;
            pag_content += '<tr>' +
            '<th onclick="sortTable(0)">ID</th>' +
            '<th onclick="sortTable(1)">Name</th>' +
            '<th>Detail</th>' +
            '<th>Edit</th>' +
            '<th>Remove</th>' +
            '</tr>';
            docs.forEach(function(element, key, array) {
                pag_content += '<tr>' +
                '<td>' + docs[key]._id + '</td>' +
                '<td>' +
                '<div class="row">' +
                '<div class="col-md-2">' +
                '<div class="product-image">' +
                '<img src="' + docs[key].image[0] + '" alt="Product Image">' +
                '</div>' +
                '</div>' +
                '<div class="col-md-10">' +
                '<div class="product-name">' +
                docs[key].name + '<span class="product-brand">(' + docs[key].brand + ')</span>' +
                '</div>' +
                '<div class="product-kind">' +
                docs[key].kind +
                '</div>' +
                '</div>' +
                '</div>' +
                '</td>' + 
                '<td><a href="/products/' + docs[key]._id + '" rel="tooltip" title="View this product detail" data-placement="bottom"><i class="fa fa-eye"></i></a></td>' +
                '<td><a href="/admin/editproduct/' + docs[key]._id + '" rel="tooltip" title="Edit this product" data-placement="bottom"><i class="fa fa-edit"></i></a></td>' + 
                '<td><a href="/admin/removeproduct/' + docs[key]._id + '" rel="tooltip" title="Remove this product" data-placement="bottom"><i class="fa fa-remove"></i></a></td>' +
                '</tr>';
            });

            var no_of_paginations = Math.ceil(count / per_page);

            var start_loop, end_loop;

            if (cur_page >= 7) {
                start_loop = cur_page - 3;
                if (no_of_paginations > cur_page + 3)
                    end_loop = cur_page + 3;
                else if (cur_page <= no_of_paginations && cur_page > no_of_paginations - 6) {
                    start_loop = no_of_paginations - 6;
                    end_loop = no_of_paginations;
                } else {
                    end_loop = no_of_paginations;
                }
            } else {
                start_loop = 1;
                if (no_of_paginations > 7)
                    end_loop = 7;
                else
                    end_loop = no_of_paginations;
            }

            pag_navigation += "<ul>";

            if (first_btn && cur_page > 1) {
                pag_navigation += "<li p='1' class='active'>First</li>";
            } else if (first_btn) {
                pag_navigation += "<li p='1' class='inactive'>First</li>";
            } 

            if (previous_btn && cur_page > 1) {
                pre = cur_page - 1;
                pag_navigation += "<li p='" + pre + "' class='active'>Previous</li>";
            } else if (previous_btn) {
                pag_navigation += "<li class='inactive'>Previous</li>";
            }
            for (i = start_loop; i <= end_loop; i++) {

                if (cur_page == i)
                    pag_navigation += "<li p='" + i + "' class = 'selected' >" + i + "</li>";
                else
                    pag_navigation += "<li p='" + i + "' class='active'>" + i + "</li>";
            }

            if (next_btn && cur_page < no_of_paginations) {
                nex = cur_page + 1;
                pag_navigation += "<li p='" + nex + "' class='active'>Next</li>";
            } else if (next_btn) {
                pag_navigation += "<li class='inactive'>Next</li>";
            }

            if (last_btn && cur_page < no_of_paginations) {
                pag_navigation += "<li p='" + no_of_paginations + "' class='active'>Last</li>";
            } else if (last_btn) {
                pag_navigation += "<li p='" + no_of_paginations + "' class='inactive'>Last</li>";
            }

            pag_navigation = pag_navigation + "</ul>";

            var response = {
                'content': pag_content,
                'navigation' : pag_navigation
            };

            res.json(response);
        })
})
},

removeProduct: function(req, res, next) {
    var id = req.params.id;
    mongoose.connect('mongodb://thegearsweb:ngunguoi@ds143330.mlab.com:43330/myproducts', function(err, db) {
        assert.equal(null, err);
        db.collection('products').deleteOne({"_id": objectId(id)}, function(err, result) {
            assert.equal(null, err);
            db.close();
        })
    });
    res.redirect('/admin/productlist');
},

showEditProduct: function(req, res, next)  {
    Product.findById(req.params.id, function(err, product) {
        if (err)  
            return next(err);
        Product.findALlSubcategoriesByKind(product.kind).then(function(docs){
            for(var i = 0; i < docs.length; i++){
                product.subcategories.find(function(value, index){
                    if(value === docs[i])
                    {   
                        console.log(index);
                        docs.splice(i , 1);
                        i--;
                    }
                })
            };
            var isLaptop;
            if (product.kind == "Laptop") 
                isLaptop = true;
            else 
            {
                isLaptop = false;
                product.desc = product.description.join('\n');
            }
            console.log(product.desc);
            var object = {product: product, subcategories: docs, isLaptop: isLaptop, layout: 'admin-layout.hbs', isFullImg: product.image.length >= 4};
            kindHelper(object, function(){
                res.render('admin/admin-editproduct', object);    
            })
        });       
    });
},

editProduct: function(req, res, next) {
    var product;
    product = new Product({
        _id : req.params.id,
        name : req.body.name,
        brand: req.body.brand,
        subcategories: req.body.subcategories,
        cpu: req.body.cpu,
        ram: req.body.ram,
        rom: req.body.rom,
        price: req.body.price,
        weight: req.body.weight,
        resolution: req.body.resolution,
        quantity: req.body.quantity,
        gpu: req.body.gpu
    });
    if(req.body.description)
        product.description= req.body.description.split('\n');
    Product.findById(req.params.id, function(err, prod) {
        if (err)  
            return next(err);
        product.image = prod.image;
        req.files.filter(function (file) {
            product.image[file.filename.split('-')[0]] = '/uploads/' + file.filename;
        });
        Product.findByIdAndUpdate(req.params.id, {$set: product}, {}, function (err) {
            if (err)
                return next(err); 
            res.redirect('/admin/editProduct/' + req.params.id);
        });
    });
},
showProduct: function(req, res, next)  {
    var kind = req.params.id;
    var subcategories;
    Product.findALlSubcategoriesByKind(kind).then(function(docs){
        if (kind == "Laptop") 
            isLaptop = true;
        else 
        {
            isLaptop = false;
        }
        var object = {subcategories: docs, isLaptop: isLaptop, layout: 'admin-layout.hbs'};
        kindHelper(object, function(){
            res.render('admin/admin-addproduct', object);    
        })
    });       
},
addProduct: function(req, res, next) {
    var product;
    product = new Product({
        name : req.body.name,
        brand: req.body.brand,
        subcategories: req.body.subcategories,
        cpu: req.body.cpu,
        ram: req.body.ram,
        rom: req.body.rom,
        price: req.body.price,
        weight: req.body.weight,
        kind: req.params.id,
        resolution: req.body.resolution, 
        gpu: req.body.gpu,
        quantity: req.body.quantity,
    });
    console.log(req.files);
    req.files.filter(function (file) {
        product.image.push('/uploads/' + file.filename);
    });
    if (req.body.description)
        product.description= req.body.description.split('\n');
    product.save(function (err) {
        if (err)
            return next(err); 
        res.redirect('/admin/productlist');
    });
},
getAddprodImg: function(req, res, next){
    var object = {layout: 'admin-layout.hbs'};
    kindHelper(object, function(){
        res.render('admin/admin-addproductimage', object);
    })
},
addprodImg: function(req, res, next)
{
    Product.findById(req.params.id, function(err, prod) {
        if (err)  
            return next(err);
        prod.image.push('/uploads/' + req.file.filename);
        prod.save(function (err) {
            if (err)
                return next(err); 
            res.redirect('/admin/editProduct/' + req.params.id);
        });
    });
}
};

function timeago(time) {
    return moment(time).startOf('second').fromNow();
}
