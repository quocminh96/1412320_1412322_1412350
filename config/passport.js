var passport = require('passport');
var User = require('../models/user');
var LocalStrategy = require('passport-local').Strategy;
var verification = require('./email-verification-config')

passport.serializeUser(function(user, done) {
	done(null, user.id);
})

passport.deserializeUser(function(id, done) {
	User.findById(id, function(err, user) {
		done(err, user);
	});
});

passport.use('local.signup', new LocalStrategy({
	usernameField: 'email',
	passwordField: 'password',
	passReqToCallback: true
}, function(req, email, password, done) {
	User.findOne({'email': email}, function(err, user) {
		if(err) {
			return done(err);
		}
		if (user) {
			return done(null, false, {message: 'Email is already in use!!'});
		}
		if (req.body.password != req.body.confirmpassword)
		{
			return done(null, false, {message: 'Confirm password does not match!!'});
		}

		var newUser = new User();
		newUser.email = email;
		newUser.name = req.body.name;
		newUser.address = req.body.address;
		newUser.phone = req.body.phone;
		newUser.avatar = req.body.avatar;
		newUser.password = newUser.encryptPassword(password);	
		verification.verifyEmail(newUser).then(function(mess){
			return done(null, mess);
		})
	});
	
}));

passport.use('local.signin', new LocalStrategy({
	usernameField: 'email',
	passwordField: 'password',
	passReqToCallback: true
}, function(req, email, password, done) {
	User.findOne({'email': email}, function (err, user) {
		if (err) {
			return done(err);
		}
		if (!user) {
			return done(null, false, {message: 'No user found!'});
		}
		if (!user.validPassword(password)) {
			return done(null, false, {message: 'Wrong password!'});
		}
		return done(null, user);    
	});
})); 