var User = require('../models/user');
var TempUser = require('../models/tempuser');
var mongoose = require('mongoose');
var nev = require('email-verification')(mongoose);

nev.configure({
    verificationURL: 'https://thegearsshop.herokuapp.com/email-verification/${URL}',
    persistentUserModel: User,
    tempUserModel: TempUser,

    transportOptions: {
        host: 'smtp.gmail.com',
        port: 465,
        secure: true,
        auth: {
            user: 'thegearsweb@gmail.com',
            pass: 'ngunguoi'
        }
    },
    verifyMailOptions: {
        from: 'Do Not Reply <thegearsweb@gmail.com>',
        subject: 'Please confirm account',
        html: 'Click the following link to confirm your account:</p><p>${URL}</p>',
        text: 'Please confirm your account by clicking the following link: ${URL}'
    }
}, function(error, options){
});

module.exports = {
    verifyEmail: function(newUser){
        return new Promise(function(resolve, reject) {
            nev.createTempUser(newUser, function(err, existingPersistentUser, newTempUser) {
                if (err)
                    reject(err);
                if (newTempUser) {
                    var URL = newTempUser[nev.options.URLFieldName];
                    nev.sendVerificationEmail(newTempUser.email, URL, function(err, info) {
                        if (err) {
                            reject(err);
                        }
                        resolve('A verification email has been sent to your email address');
                    });
                }
                else
                    resolve('This email address has already been registered but not yet been verified'); 
            });
        })       
    },
    checkverify: function(req){
        return new Promise(function(resolve, reject) {
            nev.confirmTempUser(req.params.url, function(err, user) {
                if (user) {
                  nev.sendConfirmationEmail(user.email, function(err, info) {
                    if (err) {
                      reject(err);
                  }
                  resolve(user);
              });
              } else {
                  reject('Link is invalid!');
              }
          });
        })
    }
};
