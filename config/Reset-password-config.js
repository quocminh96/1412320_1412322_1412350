var User = require('../models/user');
var TempUser = require('../models/tempuser');
var mongoose = require('mongoose');
var nodemailer = require('nodemailer');
var crypto = require('crypto');


var smtpTransport = nodemailer.createTransport({
	host: 'smtp.gmail.com',
	port: 465,
	secure: true,
	auth: {
		user: 'thegearsweb@gmail.com',
		pass: 'ngunguoi'
	}
});
function generateToken(){	
	return new Promise(function(resolve, reject){
		crypto.randomBytes(20, function(err, buf) {
			var token = buf.toString('hex');
			resolve(token);
		})
	});
};

module.exports = {
	sendResetPassEmail: function(email) {
		var token;
		generateToken().then(function(result){
			token = result;
		});
		return new Promise(function(resolve, reject){
			User.findOne({email: email}, function(err, docs){
				if (!docs)
				{
					console.log('loi');
					reject('This email address has not been registered!');
				}
				else
				{
					console.log(docs.email);
					docs.resetPasswordToken = token;
					docs.save(function(err)
					{
						if(err)
							reject(err);
					});
					var mailOptions = {
						to: docs.email,
						from: 'thegearsweb@gmail.com',
						subject: 'Password Reset',
						text: 'You are receiving this because you (or someone else) have requested the reset of the password for your account.\n\n' +
						'Please click on the following link, or paste this into your browser to complete the process:\n\n' +
						'https://thegearsshop.herokuapp.com/resetpassword/' + token + '\n\n' +
						'If you did not request this, please ignore this email and your password will remain unchanged.\n'
					};
					smtpTransport.sendMail(mailOptions, function(err) {
						if (err)
						{
							console.log(err);
							reject(err);
						}
						else
						{
							console.log('success');
							resolve('A reset link has been sent to ' + docs.email);
						}
					});
				}
			});
		});
	}
}