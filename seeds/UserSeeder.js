/**
 * Created by Day on 10/05/2017.
 */
var User = require('../models/user');

var mongoose = require('mongoose');
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://thegearsweb:ngunguoi@ds143330.mlab.com:43330/myproducts');

var usersData = [
	new User(
		{
			name: 'Lê Quốc Minh',
			email: 'quocminh97@gmail.com',
			phone: '01692422284',
			password: '$2a$10$JW8.uAlKmP/CBaRvF4yXVerRm5lE2920JpX8eUbLZXWPSDFBx45ia',
			avatar: 'http://cdn.epicstream.com/assets/uploads/newscover/760x400/1481221316parkerze.jpg',
			address: 'Trà Vinh',
			isAdmin: true,		
		}),
		new User(
		{
			name: 'Lương Nhật Minh',
			email: 'nhatminh190496@gmail.com',
			phone: '0905355479',
			password: '$2a$10$ancIOR4GQOdglipaVGBwCefUcLB0TXS7m9dhV5j4KE8CTilgXeFn2',
			avatar: 'https://s-media-cache-ak0.pinimg.com/originals/d3/29/2d/d3292dc26ab7e16023214bddf5213dc7.jpg',
			address: 'Tuy Hòa',
			isAdmin: true,			
		}),
		new User(
		{
			name: 'Nguyễn Thị Bảo Ngọc',
			email: 'ngoc120742@gmail.com',
			phone: '01685573742',
			password: '$2a$05$U/JpmxT0qIBluxjtsM/U9OHdqd3qjb2UnrUa9PfiyuXSgM3oKYiWu',
			avatar: 'http://www.indiewire.com/wp-content/uploads/2016/06/black-widow-marvel-standalone-movie.jpg',
			address: 'Quảng Trị',
			isAdmin: true,				
		}),
		new User(
		{
			name: 'Lương Nhật Minh',
			email: 'minhluong96@gmail.com',
			phone: '01265950888',
			password: '$2a$10$TGk.e/z2tkjOEsVRWRswF.ZRSs62GWWmnVB4/39U3GYR5iIWPK6gq',
			avatar: 'https://www.sideshowtoy.com/wp-content/uploads/2014/07/902224-User-thumb.jpg',
			address: 'TP Hồ Chí Minh',
			isAdmin: false,				
		})
];
// userSchema.methods.encryptPassword = function(password) {
// 	return bcrypt.hashSync(password, bcrypt.genSaltSync(5), null);
// };

// userSchema.methods.validPassword = function(password) {
// 	return bcrypt.compareSync(password, this.password);
// };
//Export model
var finish = 0;
mongoose.connection.on('open', function () {
    console.log('Connected');
    for (var i = 0; i < usersData.length; i++) {
        console.log(usersData[i]);
        usersData[i].save(function (err, result) {
            finish++;
            if (finish === usersData.length) {
                exit();
            }
        });
    }
})


function exit() {
    mongoose.disconnnect;
}